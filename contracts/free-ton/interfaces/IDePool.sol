/* Interface IDePool */

pragma ton-solidity >= 0.37.0;

interface IDePool {

  // Describes vesting or lock stake
  struct InvestParams {
    // Remaining size of vesting/lock stake
    uint64 remainingAmount;
    // Unix time in seconds of last payment
    uint64 lastWithdrawalTime;
    // Period in seconds after which `withdrawalValue` nanotons are unlocked
    uint32 withdrawalPeriod;
    // Amount of nanotons which are unlocked after `interval` second
    uint64 withdrawalValue;
    // Address of creator of vesting/lock
    address owner;
  }

  function getDePoolInfo() external view returns (
        bool poolClosed,
        uint64 minStake,
        uint64 validatorAssurance,
        uint8 participantRewardFraction,
        uint8 validatorRewardFraction,
        uint64 balanceThreshold,

        address validatorWallet,
        address[] proxies,

        uint64 stakeFee,
        uint64 retOrReinvFee,
        uint64 proxyFee
    );

    function getParticipants() external view returns (address[] participants);

    function getParticipantInfo(address addr) external view
        returns (
            uint64 total,
            uint64 withdrawValue,
            bool reinvest,
            uint64 reward,
            mapping (uint64 => uint64) stakes,
            mapping (uint64 => InvestParams) vestings,
            mapping (uint64 => InvestParams) locks,
            address vestingDonor,
            address lockDonor
        );

    function addOrdinaryStake(uint64 stake) external;
    /*
    function withdrawFromPoolingRound(uint64 withdrawValue) external;
    function withdrawPart(uint64 withdrawValue) external;
    function withdrawAll() external;
    */
    function addVestingStake(uint64 stake, address beneficiary,
                             uint32 withdrawalPeriod,
                             uint32 totalPeriod)
      external ;

    function setVestingDonor(address donor) external ;

}
