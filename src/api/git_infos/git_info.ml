
let raw_commit_hash = "$Format:%H$"

let commit_hash =
  if
    String.equal
      raw_commit_hash
      ("$Format:" ^ "%H$" (*trick to avoid git-subst*))
  then Generated_git_info.commit_hash
  else raw_commit_hash

let raw_abbreviated_commit_hash = "$Format:%h$"

let abbreviated_commit_hash =
  if String.equal raw_abbreviated_commit_hash ("$Format:" ^ "%h$") then
    Generated_git_info.abbreviated_commit_hash
  else raw_abbreviated_commit_hash

let raw_committer_date = "$Format:%cI$"

let committer_date =
  if String.equal raw_committer_date ("$Format:" ^ "%cI$") then
    Generated_git_info.committer_date
  else raw_committer_date

let branch = Generated_git_info.branch

let build_date = Generated_git_info.build_date
