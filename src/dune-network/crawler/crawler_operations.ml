
open Lwt_utils
open Data_types
open Dune.Types
open Db_common
open Crawler_helper

let is_usefull_operation = function
  | NTransaction _ | NOrigination _ -> true
  | _ -> false

let register_operation op_counter dbh block ~operation_hash ~index op =
  let> keep = Crawler_helper.keep_operation op in
  if not keep then return_u
  else
    let success = Crawler_helper.is_success op in
    Format.printf "  | register  %s [%d]@." operation_hash index;
    Db.register_block_operation dbh
      ~block_hash:block.block_hash
      ~operation_hash ~index ~success
      op
    >>= function
    | `AlreadyKnown -> return_u
    | `New -> incr op_counter; return_u

let register_operations dbh block operations =
  let op_counter = ref 0 in
  let>! () =
    Lwt_list.iter_s (fun op ->
      let operation_hash = op.node_op_hash in
      Lwt_list.iteri_s (fun index op ->
          (* Put connection here if concurrent updates *)
          (* let>>> dbh = () in *)
          register_operation op_counter dbh block
            ~operation_hash
            ~index op
        ) op.node_op_contents
    ) operations in
  Format.printf "  ↳ registered %d relevant operations@." !op_counter;
  ()


let update_block_operations ?(main=true) dbh ~block_hash ~level =
  let> confirmations_needed = Config.get_confirmations () in
  let> block_ops =
    Db.get_block_operations ~confirmations_needed dbh ~main ~block_hash ~level in
  Lwt_list.iter_s (fun (confirmed, success, block_hash, op_hash, op_index, dune_op) ->
      if not success then Lwt.return_unit
      else if not main then Lwt.return_unit
      else
        handle_generic_operation dbh ~confirmed ~block_hash ~op_hash ~op_index dune_op
    ) block_ops

let update_operations_info ?(main=true) dbh updated_operations =
  if not main then return_ok ()
  else
    Res.iter_s (fun (op, success, dune_op) ->
        if not success then
          let err_msg = match Option.map Crawler_helper.op_metadata dune_op with
            | None | Some None -> "Failed"
            | Some Some metadata ->
              Json_encoding.construct
                Dune.Encoding.Operation.op_metadata_encoding metadata
              |> Ezjsonm.value_to_string in
          Db.mark_operation_error op.op_id err_msg
            `Temporary
            ~retry_nb:0 (* TODO: Config.retry_attempts *)
        else
          match op.op_status with
           | Queued | Pending _ | Errored _ -> Lwt.return_ok ()
           | Unconfirmed { confirmations = 0; _ } ->
             (* Newly successful unconfirmed operation *)
             Unconfirmed.register_new_operation dbh op dune_op
           | Confirmed _ ->
             (* Newly confirmed operation *)
             Confirmed.register_new_operation dbh op dune_op
           | Unconfirmed _ ->
             (* confirmations > 0 *)
             Lwt.return_ok ()
      ) updated_operations
