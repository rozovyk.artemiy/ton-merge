(**************************************************************************)
(*                                                                        *)
(*    Copyright 2021 OCamlPro                                             *)
(*                                                                        *)
(*  All rights reserved. This file is distributed under the terms of the  *)
(*  GNU Lesser General Public License version 2.1, with the special       *)
(*  exception on linking described in the file LICENSE.                   *)
(*                                                                        *)
(**************************************************************************)

open Lwt_utils
open Data_types

let read_json enc file =
  try
    let s = EzFile.read_file file in
    EzEncoding.destruct enc s
  with exn ->
    Format.eprintf "Could not read or parse file %s: @[<hv>%a@]@."
      file (Json_encoding.print_error ?print_unknown:None) exn;
    raise exn

let set_config ~filename =

  let { network_url ;
        reveal_secrets ;
        root_address ;
        _
      } = read_json freeton_config_enc filename in
  let network_url = match network_url with
    | "" -> Freeton.tonos_url
    | _ -> network_url in
  let client = Ton_sdk.CLIENT.create network_url in
  let> result =
    Ton_sdk.ACTION.call_lwt ~client ~server_url:network_url
      ~address:root_address ~abi:Freeton.abi_DuneRootSwap
      ~meth:"getConfig" ~params:"{}" ~local:true
      ?keypair:Freeton.keypair ()
  in
  match result with
  | Error exn ->
    Printf.eprintf "set_config: exception %s\n%!" (Printexc.to_string exn);
    exit 2
  | Ok result ->
    let {
      Freeton.TYPES.merge_expiration_date ;
      swap_expiration_time ;
    } = EzEncoding.destruct Freeton.TYPES.getConfig_reply_enc result in
    let merge_expiration_date =
      Int64.of_string merge_expiration_date in
    let config = {
      Data_types.network_url ;
      root_address ;
      reveal_secrets ;
      merge_expiration_date ;
      swap_expiration_time = Int64.of_string swap_expiration_time ;
      relay_pubkey = ( match Freeton.keypair with
          | None ->
            Printf.eprintf "TON_MERGE_PASSPHRASE must be defined\n%!";
            exit 2
          | Some key_pair -> key_pair.public);
    } in
    let> () = Db.CONFIG.set config in
    Lwt.return config

let get_config () =
  let> config = Db.CONFIG.get () in
  match config with
  | Some config ->
    Printf.eprintf "Current config: \n%!";
    Printf.eprintf "   network_url  : %s\n%!" config.network_url ;
    Printf.eprintf "   relay_pubkey : %s\n%!" config.relay_pubkey ;
    Printf.eprintf "   root_address : %s\n%!" config.root_address ;
    Printf.eprintf "   merge_expiration_date : %Ld\n%!"
      config.merge_expiration_date ;
    Printf.eprintf "   merge_expiration_time : %Ld\n%!"
      ( Int64.sub config.merge_expiration_date
          (Int64.of_float @@ Unix.gettimeofday () ) );
    Printf.eprintf "   swap_expiration_time: %Ld\n%!"
      config.swap_expiration_time;
    Lwt.return config
  | None ->
    Printf.eprintf "No config. You must call with --root ROOT.\n%!";
    exit 2
