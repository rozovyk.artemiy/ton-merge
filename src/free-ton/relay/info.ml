(**************************************************************************)
(*                                                                        *)
(*    Copyright 2021 OCamlPro                                             *)
(*                                                                        *)
(*  All rights reserved. This file is distributed under the terms of the  *)
(*  GNU Lesser General Public License version 2.1, with the special       *)
(*  exception on linking described in the file LICENSE.                   *)
(*                                                                        *)
(**************************************************************************)

open EzCompat
open Lwt_utils
open Data_types
open Monitor_database.TYPES

let cut v =
  let rem = Int64.rem v 1_000L in
  let v = Int64.div v 1_000L in
  v, rem

let string_of_nanoton v =
  let v = Z.to_int64 v in
  let v, nanotons = cut v in
  let v, mutons = cut v in
  let v, millitons = cut v in
  let v, tons = cut v in
  let v, thousandtons = cut v in
  let v, milliontons = cut v in
  let v, billiontons = cut v in
  assert (v = 0L);
  let tons =
    match billiontons, milliontons, thousandtons with
    | 0L, 0L, 0L -> Int64.to_string tons
    | 0L, 0L, _ -> Printf.sprintf "%Ld_%03Ld" thousandtons tons
    | 0L, _, _ -> Printf.sprintf "%Ld_%03Ld_%03Ld" milliontons thousandtons tons
    | _, _, _ -> Printf.sprintf "%Ld_%03Ld_%03Ld_%03Ld"
                   billiontons milliontons thousandtons tons
  in
  let nanotons = match nanotons, mutons, millitons with
    | 0L, 0L, 0L -> ""
    | 0L, 0L, _ -> Printf.sprintf "%03Ld" millitons
    | 0L, _, _ -> Printf.sprintf "%03Ld_%03Ld" millitons mutons
    | _, _, _ -> Printf.sprintf "%03Ld_%03Ld_%03Ld" millitons mutons nanotons
  in
  let s = Printf.sprintf "%s.%s" tons nanotons in
  s

let shorten_relay s =
  String.sub s 0 4


let call_get config ~contract ~address ~abi ~meth =

  let> result =
    Ton_sdk.ACTION.call_lwt ~server_url:config.network_url
      ~address ~abi ~meth ~params:"{}" ~local:true
      ()
  in
  begin
    match result with
    | Error exn ->
      Printf.eprintf "%s.%s(): exception %s\n%!" contract meth
        (Printexc.to_string exn);
    | Ok result ->
      Printf.printf "%s.%s(): %s\n%!" contract meth result ;
  end;
  Lwt.return_unit


let merge_info () =

  let> config = Config.get_config () in

  let> () = Monitor_database.update_swaps ~verbose:false () in

  let refunded_nswaps = ref 0 in
  let refunded_duns = ref Z.zero in

  let active_nswaps = ref 0 in
  let active_duns = ref Z.zero in
  let active_tons = ref Z.zero in

  let complete_nswaps = ref 0 in
  let complete_duns = ref Z.zero in
  let complete_tons = ref Z.zero in

  let active_swaps = ref [] in

  Hashtbl.iter (fun _ ss ->
      let s = ss.swap in
      match s.dune_status, s.freeton_status with
      | SwapRefunded, SwapCancelled ->
        incr refunded_nswaps ;
        refunded_duns := Z.add !refunded_duns s.dun_amount
      | SwapCompleted, SwapTransferred ->
        incr complete_nswaps ;
        complete_duns := Z.add !complete_duns s.dun_amount ;
        complete_tons := Z.add !complete_tons s.ton_amount ;
      | _ ->
        incr active_nswaps ;
        active_duns := Z.add !active_duns s.dun_amount ;
        active_tons := Z.add !active_tons s.ton_amount ;
        active_swaps := ss :: !active_swaps ;
    ) Monitor_database.swaps_table ;


  let active_swaps =
    let t = Array.of_list !active_swaps in
    Array.sort (fun s1 s2 -> compare s1.swap.swap_id s2.swap.swap_id) t ;
    t
  in

  Printf.printf "Active swaps:\n%!";
  Array.iter (fun ss ->
      let s = ss.swap in
      let date = CalendarLib.Printer.Calendar.sprint "%Y-%m-%dT%H:%M:%S"
          s.time in
      let confirms =
        match Hashtbl.find Monitor_database.confirmations_table s.swap_id with
        | exception Not_found -> ""
        | conf ->
          let list = StringSet.to_list conf.relay_pubkeys in
          let n = List.length list in
          Printf.sprintf "%d confs [%s]"
            n ( String.concat " " ( List.map shorten_relay list ))
      in
      let addr =
        match s.swap_hash with
        | None -> "No hash"
        | Some swap_hash ->
          match Hashtbl.find Monitor_database.addresses_table swap_hash with
          | exception Not_found -> "No address"
          | s -> s.address_address
      in
      Printf.printf "%4d: %10s %30s %20s %20s %s %s %s\n" s.swap_id
        ( Encoding_common.string_of_dune_status s.dune_status )
        ( Encoding_common.string_of_freeton_status s.freeton_status )
        date
        ( string_of_nanoton s.ton_amount )
        ( match s.secret with None -> "???" | Some s ->
              Printf.sprintf "SEC %S" (Bytes.to_string s))
        confirms
        addr
      ;

    ) active_swaps ;

  Printf.printf "Refunded: %d swaps (%s DUNs)\n%!"
    !refunded_nswaps (Encoding_common.string_of_amount !refunded_duns);
  Printf.printf "Complete: %d swaps (%s DUNs, %s TONs)\n%!"
    !complete_nswaps
    (Encoding_common.string_of_amount !complete_duns)
    (string_of_nanoton !complete_tons)
  ;
  Printf.printf "Active: %d swaps (%s DUNs, %s TONs)\n%!"
    !active_nswaps
    (Encoding_common.string_of_amount !active_duns)
    (string_of_nanoton !active_tons)
  ;


  let> () = call_get config
      ~address:config.root_address ~abi:Freeton.abi_DuneRootSwap
      ~meth:"get" ~contract:"DuneRootSwap"
  in

  let> () = call_get config
      ~address:config.root_address ~abi:Freeton.abi_DuneRootSwap
      ~meth:"remainingTime" ~contract:"DuneRootSwap"
  in
  Lwt.return_unit

let swap_info ~swap_id =
  let> config = Config.get_config () in
  let> s = Db.SWAPS.get ~swap_id in
  match s with
  | None -> failwith "No such swap"
  | Some s ->
    let json = EzEncoding.construct ~compact:false Encoding_common.swap s in
    Printf.printf "%s\n%!" json ;
    match s.swap_hash with
    | None ->
      Printf.printf "Hash not computed\n%!";
      Lwt.return_unit
    | Some swap_hash ->
      let> r = Db.CONTRACTS.get ~swap_hash in
      match r with
      | None ->
        Printf.printf "Not yet deployed\n%!";
        Lwt.return_unit
      | Some address ->
        Printf.printf "Deployed: %s\n%!" address ;
        let> () = call_get config
            ~address ~abi:Freeton.abi_DuneUserSwap
            ~meth:"getOrderRemainingTime" ~contract:"DuneUserSwap"
        in
        let> () = call_get config
            ~address ~abi:Freeton.abi_DuneUserSwap
            ~meth:"get" ~contract:"DuneUserSwap"
        in
        Lwt.return_unit
