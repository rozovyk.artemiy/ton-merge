(**************************************************************************)
(*                                                                        *)
(*    Copyright 2021 OCamlPro                                             *)
(*                                                                        *)
(*  All rights reserved. This file is distributed under the terms of the  *)
(*  GNU Lesser General Public License version 2.1, with the special       *)
(*  exception on linking described in the file LICENSE.                   *)
(*                                                                        *)
(**************************************************************************)

open Lwt_utils
open Data_types
open Monitor_database.TYPES

let z45 = Z.of_int 45
let z1000 = Z.of_int 1000
let z100 = Z.of_int 100
let z111 = Z.of_int 111
let z1000000 = Z.of_int 1_000_000
let z1000000000 = Z.of_int 1_000_000_000

let string_of_dun z =
  let dun, milli = Z.div_rem z z1000000 in
  let milli = Z.to_string milli in
  let milli = (String.make ( 6 - String.length milli ) '0' ) ^ milli in
  Printf.sprintf "%s.%s" (Z.to_string dun) milli

let string_of_ton z =
  let ton, nano = Z.div_rem z z1000000000 in
  let nano = Z.to_string nano in
  let nano = (String.make ( 9 - String.length nano ) '0' ) ^ nano in
  Printf.sprintf "%s.%s" (Z.to_string ton) nano


let print_dun total name =
  if !(fst total) > Z.zero then
    Printf.printf "   total %s : %d swaps, %s DUN\n%!" name
      (!(snd total))
      (string_of_dun !(fst total))

let print_ton total name =
  if !(fst total) > Z.zero then
    Printf.printf "   total %s : %d swaps, %s TON\n%!" name
      (!(snd total))
      (string_of_ton !(fst total))

let ref_init () = (ref Z.zero, ref 0)

let add x y =
  fst x := Z.add !(fst x) y;
  incr (snd x)

let watch_db () =
  let> config = Config.get_config () in

  let print_stats ~print_initial_state () =

    let active_swaps = ref 0 in

    (* tons *)
    let total_dun_completed = ref_init () in
    let total_dun_can_reveal = ref_init () in
    let total_dun_confirmed = ref_init () in
    let total_dun_submitted = ref_init () in
    let total_dun_expired = ref_init () in
    let total_dun_refunded = ref_init () in
    let total_dun_refund_asked = ref_init () in

    let total_ton_waiting_for_credit = ref_init () in
    let total_ton_credited = ref_init () in
    let total_ton_waiting_for_confirmation = ref_init () in
    let total_ton_fully_confirmed = ref_init () in
    let total_ton_credit_denied = ref_init () in
    let total_ton_revealed = ref_init () in
    let total_ton_waiting_for_depool = ref_init () in
    let total_ton_depool_denied = ref_init () in
    let total_ton_transferred = ref_init () in
    let total_ton_cancelled = ref_init () in
    Hashtbl.iter (fun _ ss ->
        let s = ss.swap in
        incr active_swaps;
        begin
          match s.dune_status with
          | SwapCompleted ->
            add total_dun_completed s.dun_amount;
            decr active_swaps
          | SwapCanReveal ->
            add total_dun_can_reveal s.dun_amount;
          | SwapRefundAsked ->
            add total_dun_refund_asked s.dun_amount;
          | SwapExpired ->
            add total_dun_expired s.dun_amount
          | SwapConfirmed ->
            add total_dun_confirmed s.dun_amount;
          | SwapSubmitted ->
            add total_dun_submitted s.dun_amount;
          | SwapRefunded ->
            decr active_swaps ;
            add total_dun_refunded s.dun_amount
        end;
        begin match s.freeton_status with
          | SwapWaitingForCredit ->
            add total_ton_waiting_for_credit s.ton_amount
          | SwapWaitingForConfirmation ->
            add total_ton_waiting_for_confirmation s.ton_amount
          | SwapFullyConfirmed ->
            add total_ton_fully_confirmed s.ton_amount
          | SwapCreditDenied ->
            add total_ton_credit_denied s.ton_amount
          | SwapRevealed ->
            add total_ton_revealed s.ton_amount
          | SwapWaitingForDepool ->
            add total_ton_waiting_for_depool s.ton_amount
          | SwapDepoolDenied ->
            add total_ton_depool_denied s.ton_amount
          | SwapTransferred ->
            add total_ton_transferred s.ton_amount
          | SwapCancelled ->
            add total_ton_cancelled s.ton_amount
          | SwapCredited _ ->
            add total_ton_credited s.ton_amount
        end;
        if print_initial_state then
          begin
            match s.freeton_status with
            | SwapTransferred -> ()
            | _ ->
              Printf.printf "swap %d: %s DUN %s TON\n%!"
                s.swap_id
                ( string_of_dun s.dun_amount )
                ( string_of_ton s.ton_amount );
              Printf.printf "   statuses: %s %s\n%!"
                ( Encoding_common.string_of_dune_status s.dune_status )
                ( Encoding_common.string_of_freeton_status s.freeton_status ) ;
          end
      ) Monitor_database.swaps_table;

    Printf.printf "=============================================\n%!";
    Printf.printf "%d active swaps\n%!" !active_swaps;

    Printf.printf "Dune side:\n%!";
    print_dun total_dun_submitted  "submitted" ;
    print_dun total_dun_confirmed  "confirmed" ;
    print_dun total_dun_can_reveal  "can reveal" ;
    print_dun total_dun_completed  "completed" ;
    print_dun total_dun_expired  "expired" ;
    print_dun total_dun_refund_asked  "refund asked" ;
    print_dun total_dun_refunded  "refunded" ;

    Printf.printf "FreeTON side:\n%!";
    print_ton total_ton_waiting_for_confirmation  "waiting for confirms" ;
    print_ton total_ton_fully_confirmed  "confirmed" ;
    print_ton total_ton_waiting_for_credit  "waiting for credit" ;
    print_ton total_ton_credited  "credited" ;
    print_ton total_ton_credit_denied  "credit denied" ;
    print_ton total_ton_revealed  "revealed" ;
    print_ton total_ton_waiting_for_depool  "waiting for depool" ;
    print_ton total_ton_depool_denied  "depool denied" ;
    print_ton total_ton_transferred  "transferred" ;
    print_ton total_ton_cancelled  "cancelled" ;

    let> giver_balance =
      let> result = Ton_sdk.REQUEST.post_lwt config.network_url
          (Ton_sdk.REQUEST.account ~level:1 config.root_address) in
      match result with
      | Ok [ acc ] -> begin
          match acc.acc_balance with
          | None -> Lwt.return Z.zero
          | Some z -> Lwt.return z
        end
      | Ok [] ->
        Printf.eprintf "Giver disappeared!%!";
        Lwt.return Z.zero
      | Ok _ -> assert false
      | Error exn ->
        Printf.eprintf "Error while requesting giver_balance (using 0): %s\n%!" (Printexc.to_string exn);
        Lwt.return Z.zero

    in
    Printf.printf "  Root balance: %s TON %s DUN\n%!"
      ( string_of_ton giver_balance )
      ( string_of_dun ( Z.div ( Z.mul giver_balance z45) z1000 ) );
    Lwt.return_unit
  in

  Freeton.update_curtime ();
  let> () = Monitor_database.update_swaps ~verbose:false () in
  let> () = print_stats ~print_initial_state:true () in
  let swap_id_of_hash = Hashtbl.create 1000 in

  let add_swap_hash s swap_hash =
    Hashtbl.add swap_id_of_hash swap_hash s.swap_id
  in

  let rec iter () =


    Freeton.update_curtime ();
    let> () = Monitor_database.update_swaps ~verbose:false () in

    let nchanges = List.length !Monitor_database.swaps_changes in
    let> () =
      if nchanges <> 0 then begin
        Printf.printf "%d swaps changes:\n%!" nchanges ;
        List.iter (fun (old_s, ss) ->
            let s = ss.swap in
            match old_s with
            | None ->
              Printf.printf "  * %d: new swap\n" s.swap_id ;
              Printf.printf "        tokens: %s DUN %s TON\n"
                (string_of_dun s.dun_amount) (string_of_ton s.ton_amount);
              Printf.printf "        statuses: %s %s\n%!"
                ( Encoding_common.string_of_dune_status s.dune_status )
                ( Encoding_common.string_of_freeton_status s.freeton_status ) ;
              Printf.printf "  *     secret: %s\n%!"
                ( match s.secret with
                  | None -> "none"
                  | Some s -> Printf.sprintf "%S" (Bytes.to_string s)
                );
              (match s.swap_hash with
               | None -> ()
               | Some swap_hash -> add_swap_hash s swap_hash );

            | Some old_s ->
              if old_s.dune_status <> s.dune_status then
                Printf.printf "  * %d: update dune %s -> %s\n%!"
                  s.swap_id
                  ( Encoding_common.string_of_dune_status old_s.dune_status )
                  ( Encoding_common.string_of_dune_status s.dune_status ) ;
              if old_s.freeton_status <> s.freeton_status then
                Printf.printf "  * %d: update freeton %s -> %s\n%!"
                  s.swap_id
                  ( Encoding_common.string_of_freeton_status old_s.freeton_status )
                  ( Encoding_common.string_of_freeton_status s.freeton_status ) ;
              if old_s.secret <> s.secret then
                Printf.printf "  * %d: update secret %s -> %s\n%!"
                  s.swap_id
                  ( match old_s.secret with
                    | None -> "none"
                    | Some s -> Printf.sprintf "%S" (Bytes.to_string s)
                  )
                  ( match s.secret with
                    | None -> "none"
                    | Some s -> Printf.sprintf "%S" (Bytes.to_string s)
                  )
              ;
              (match old_s.swap_hash, s.swap_hash with
               | None, Some swap_hash -> add_swap_hash s swap_hash
               | _ -> ()
              );

          ) ( List.rev !Monitor_database.swaps_changes ) ;
        print_stats ~print_initial_state:false ()
      end else
        Lwt.return_unit
    in

    let nchanges = List.length !Monitor_database.addresses_changes in
    if nchanges <> 0 then begin
      Printf.printf "%d new addresses:\n%!" nchanges ;
      List.iter (fun addr ->
          match Hashtbl.find swap_id_of_hash addr.address_swap_hash with
          | exception Not_found ->
            Printf.printf "  * %s : %s\n%!"
              addr.address_swap_hash addr.address_address
          | swap_id ->
            Printf.printf "  * %d : %s\n%!"
              swap_id addr.address_address
        )  ( List.rev !Monitor_database.addresses_changes ) ;
    end;

    let nchanges = List.length !Monitor_database.confirmations_changes in
    if nchanges <> 0 then begin
      Printf.printf "%d confirmations:\n%!" nchanges ;
      List.iter (fun ( swap_id, relay ) ->
          Printf.printf "  * %d : %s\n%!" swap_id
            ( Info.shorten_relay relay )
        )  ( List.rev !Monitor_database.confirmations_changes ) ;
    end;

    let> () = Lwt_unix.sleep 3.0 in
    iter ()
  in
  iter ()
