(**************************************************************************)
(*                                                                        *)
(*    Copyright 2021 OCamlPro                                             *)
(*                                                                        *)
(*  All rights reserved. This file is distributed under the terms of the  *)
(*  GNU Lesser General Public License version 2.1, with the special       *)
(*  exception on linking described in the file LICENSE.                   *)
(*                                                                        *)
(**************************************************************************)

open EzCompat
open Lwt_utils
open Data_types

let freeton_names = [
  "api_server" ;
  "dune_injector" ;
  "dune_crawler" ;
  "monitor" ;
  "monitor_freeton" ;
  "monitor_database_deployer" ;
  "monitor_database_confirmer" ;
  "monitor_database_revealer" ;
  "monitor_database_finisher" ;
]

let checks () =
  let mergedir =
    match Sys.getenv "MERGEDIR" with
    | exception _ ->
      Printf.eprintf "Fatal error: missing environment variable MERGEDIR.\n%!";
      exit 2
    | mergedir -> mergedir
  in
  let> config = Config.get_config () in
  let> () = Lwt_list.iter_s (fun name ->
      let> () =
        Lwt.catch (fun () ->
            let> pid = Db.PID.get name in
            begin
              match pid with
              | None ->
                Printf.eprintf "%s: not running\n%!" name ;
              | Some pid ->
                Printf.eprintf "%s: running with PID %d\n%!" name pid;
            end;
            Lwt.return_unit
          )
          (fun _exn ->
             Printf.eprintf "%s: not running\n%!" name ;
             Lwt.return_unit )
      in
      let logfile = Filename.concat mergedir
          ( Printf.sprintf "logs/%s.log" name )
      in
      if Sys.file_exists logfile then begin
        let st = Unix.lstat logfile in
        let now = Unix.gettimeofday () in
        let recent = now -. st.Unix.st_mtime  in
        Printf.eprintf "  log file %s updated %.0f seconds ago \n%!"
          logfile recent
      end else
        Printf.eprintf "  no log file %s\n%!" logfile ;
      Lwt.return_unit
    )
      (
        freeton_names )
  in

  let> config = Config.get_config () in
  let network_url = config.network_url in
  let root_address = config.root_address in
  let client = Ton_sdk.CLIENT.create network_url in
  let> result =
    Ton_sdk.ACTION.call_lwt ~client ~server_url:network_url
      ~address:root_address ~abi:Freeton.abi_DuneRootSwap
      ~meth:"get" ~params:"{}" ~local:true
      ()
  in
  let relays = ref [] in
  let> () =
    match result with
    | Error exn ->
      Printf.eprintf "set_config: exception %s\n%!" (Printexc.to_string exn);
      exit 2
    | Ok result ->
      let args = Relay_misc.map_of_json result in
      match StringMap.find "relays" args with
      | exception _ ->
        assert false
      | `A list ->
        List.iter (function
              `String r ->
              let r = String.sub r 2 64 in
              relays := r :: !relays
            | _ -> assert false ) list ;
        Lwt.return_unit
      | _ -> assert false
  in
  let> () =
    Lwt_list.iter_s (fun relay ->
        Printf.eprintf "Relay: %s\n%!" relay ;
        Lwt_list.iter_s (fun kind ->
            Printf.eprintf "  %s: " ( Monitor_database.string_of_kind kind );
            let> list = Db.PINGS.list
                ~limit:6
                ~relay ~kind:(Int32.of_int kind) () in
            match list with
            | [] ->
              Printf.eprintf "never seen\n%!";
              Lwt.return_unit
            | (_, _, _, last_time) :: _ ->
              let list = List.rev list in
              let oldest_time =
                match list with
                | (_, _, _, time) :: _ -> time
                | _ -> assert false
              in
              let now = Unix.gettimeofday () |> Int64.of_float in
              let count = List.length list in
              let mean = Int64.div ( Int64.sub now oldest_time)
                  ( Int64.of_int count ) in
              Printf.eprintf " seen %Ld seconds ago (average %Ld seconds, count %d)\n%!"
                (Int64.sub now last_time ) mean count ;
              Lwt.return_unit
          )
          [0; 1; 2; 3; 4 ]
      ) !relays
  in

  Printf.printf "Checks done.\n%!";
  Lwt.return_unit
