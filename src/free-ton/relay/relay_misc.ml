(**************************************************************************)
(*                                                                        *)
(*    Copyright 2021 OCamlPro                                             *)
(*                                                                        *)
(*  All rights reserved. This file is distributed under the terms of the  *)
(*  GNU Lesser General Public License version 2.1, with the special       *)
(*  exception on linking described in the file LICENSE.                   *)
(*                                                                        *)
(**************************************************************************)

open EzCompat
open Lwt_utils

let string_of_swap s =
  EzEncoding.construct ~compact:true Encoding_common.swap s

let map_of_json ?(root=[]) json =
  let json = Ezjsonm.from_string json in
  let map = ref StringMap.empty in
  let add path s =
    let key = String.concat ":" ( List.rev path ) in
    map := StringMap.add key s !map
  in
  let rec iter path json =
    add path json;
    match json with
      `O list ->
        List.iter (fun (s,v) ->
            iter (s :: path) v
          ) list
    | `A list ->
        List.iteri (fun i v ->
            iter (string_of_int i :: path) v
          ) list
    | `Bool _
    | `Null
    | `Float _
    | `String _ -> ()
  in
  iter (List.rev root) json;
  !map

let fatal loc =
  Printf.eprintf "Fatal error at %s\n%!" loc;
  exit 2

let todo loc =
  Printf.eprintf "Fatal error TODO at %s\n%!" loc;
  exit 2
