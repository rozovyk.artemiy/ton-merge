#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH



export RELAY=1
export TON_MERGE_DB="${RELAY1_DATABASE}"

echo COMMAND ${FREETON_RELAY} --kill
cmd ${FREETON_RELAY} --kill


export RELAY=2
export TON_MERGE_DB="${RELAY2_DATABASE}"

echo COMMAND ${FREETON_RELAY} --kill
cmd ${FREETON_RELAY} --kill



export RELAY=3
export TON_MERGE_DB="${RELAY3_DATABASE}"

echo COMMAND ${FREETON_RELAY} --kill
cmd ${FREETON_RELAY} --kill
