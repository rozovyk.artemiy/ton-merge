#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH







$FT account remove root_address 

$FT contract deploy DuneRootSwap --create root_address --sign admin '{ "freeton_giver" : "%{account:address:freeton_giver}", "relays": [ "0x%{account:pubkey:relay1}",  "0x%{account:pubkey:relay2}",  "0x%{account:pubkey:relay3}" ], "nreqs": %{env:TON_NREQS}, "duneUserSwapCode": "%{get-code:contract:tvc:DuneUserSwap}","duneVestingCode": "%{get-code:contract:tvc:DuneVesting}", "merge_expiration_date": %{now:plus:%{env:SWAP_PERIOD}}, "swap_expiration_time": %{env:SWAP_EXPIRATION_TIME}, "testing": true }' || exit 2

# $FT node --give root_address:1000 || exit 2

$FT watch root_address &> watch-root.log &


$FT multisig transfer ${ROOT_TONS} --from freeton_giver --to root_address || exit 2





