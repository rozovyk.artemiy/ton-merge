#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH


$FT inspect -h --shard-account orl_swap --subst @%{res:0:id} --output orl_swap.block

$FT watch --account orl_swap &> watch-orl.log &

cmd ${FREETON_RELAY} --set-secret 1:bonjour

# to ask again for a transfer in case of failure:
# $FT call orl_swap transferOrder --sign orl
