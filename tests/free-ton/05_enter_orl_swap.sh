#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH

$FT output --file test-swap-orl.json.in -o test-swap-orl.json || exit 2


export RELAY=1
export TON_MERGE_DB="${RELAY1_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY1_PASSPHRASE}"

echo COMMAND cmd ${FREETON_RELAY} --test-swap test-swap-orl.json
cmd ${FREETON_RELAY} --test-swap test-swap-orl.json

export RELAY=2
export TON_MERGE_DB="${RELAY2_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY2_PASSPHRASE}"

echo COMMAND cmd ${FREETON_RELAY} --test-swap test-swap-orl.json
cmd ${FREETON_RELAY} --test-swap test-swap-orl.json


export RELAY=3
export TON_MERGE_DB="${RELAY3_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY3_PASSPHRASE}"

echo COMMAND cmd ${FREETON_RELAY} --test-swap test-swap-orl.json
cmd ${FREETON_RELAY} --test-swap test-swap-orl.json
