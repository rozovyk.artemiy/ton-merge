#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH



export RELAY=1
export TON_MERGE_DB="${RELAY1_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY1_PASSPHRASE}"
export MERGEDIR=$(pwd)/${RELAY}
mkdir -p ${MERGEDIR}/logs

echo COMMAND ${FREETON_RELAY} --kill
cmd ${FREETON_RELAY} --kill

echo COMMAND ${FREETON_RELAY} --daemon --only-freeton '&>' ${TON_MERGE_DB}.log 
cmd ${FREETON_RELAY} --daemon --only-freeton &> ${TON_MERGE_DB}.log  &



export RELAY=2
export TON_MERGE_DB="${RELAY2_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY2_PASSPHRASE}"
export MERGEDIR=$(pwd)/${RELAY}
mkdir -p ${MERGEDIR}/logs

echo COMMAND ${FREETON_RELAY} --kill
cmd ${FREETON_RELAY} --kill

echo COMMAND ${FREETON_RELAY} --daemon --only-freeton '&>' ${TON_MERGE_DB}.log 
cmd ${FREETON_RELAY} --daemon --only-freeton &> ${TON_MERGE_DB}.log  &



export RELAY=3
export TON_MERGE_DB="${RELAY3_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY3_PASSPHRASE}"
export MERGEDIR=$(pwd)/${RELAY}
mkdir -p ${MERGEDIR}/logs

echo COMMAND ${FREETON_RELAY} --kill
cmd ${FREETON_RELAY} --kill

echo COMMAND ${FREETON_RELAY} --daemon --only-freeton '&>' ${TON_MERGE_DB}.log 
cmd ${FREETON_RELAY} --daemon --only-freeton &> ${TON_MERGE_DB}.log  &
