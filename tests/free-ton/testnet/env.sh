
function cmd()
{
    echo
    echo  COMMAND $*
    echo
    $* || exit 2
}

function cmdk()
{
    echo
    echo  COMMAND $*
    echo
    $*
}

FT="ft --echo"

# This contract is used to manage the test of the vesting/debot only
export DEPLOYER=fabrice

# TESTUSER
export TESTUSER=test1

export SWAP_PERIOD=7:day

# one week in seconds
# export SWAP_EXPIRATION_TIME=604800
export SWAP_EXPIRATION_TIME=3600


