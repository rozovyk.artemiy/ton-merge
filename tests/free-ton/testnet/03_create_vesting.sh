#!/bin/bash

set -euo pipefail

. ./env.sh

$FT account remove vest_addr

$FT call --local root_address getVestingAddress '{ "pubkey": "0x%{account:pubkey:%{env:TESTUSER}}"}' --subst '@%{apply:create-contract:vest_addr:DuneVesting:%{res:value0}}'

$FT multisig transfer 1010 --from $DEPLOYER --to vest_addr --sponsor

# when deployed:
# $FT call --local vest_addr getCollectInfo
