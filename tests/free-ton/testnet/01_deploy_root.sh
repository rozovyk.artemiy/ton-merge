#!/bin/bash

set -euo pipefail

. ./env.sh

ft switch to testnet

$FT contract deploy DuneRootSwap --create root_address --sign $DEPLOYER '{ "freeton_giver" : "%{account:address:%{env:DEPLOYER}}", "relays": [ "0x%{account:pubkey:%{env:DEPLOYER}}" ], "nreqs": 1, "duneUserSwapCode": "%{get-code:contract:tvc:DuneUserSwap}","duneVestingCode": "%{get-code:contract:tvc:DuneVesting}", "merge_expiration_date": %{now:plus:%{env:SWAP_PERIOD}}, "swap_expiration_time": %{env:SWAP_EXPIRATION_TIME}, "testing": true }' --credit 1 --deployer $DEPLOYER -f

