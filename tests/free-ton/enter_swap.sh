#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH


# should be between 0 and 9
export TON_SWAP=$1

export RELAY=1
export TON_MERGE_DB="${RELAY1_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY1_PASSPHRASE}"

$FT output --file test-swap-10${TON_SWAP}.json.in -o test-swap-10${TON_SWAP}.json || exit 2

cmd ${FREETON_RELAY} --test-swap test-swap-10${TON_SWAP}.json

cmd ${FREETON_RELAY} --get-address 10${TON_SWAP}:orl_swap_10${TON_SWAP}.calc

$FT account --create user10${TON_SWAP}_swap --address $(cat orl_swap_10${TON_SWAP}.calc) --contract DuneUserSwap

export RELAY=2
export TON_MERGE_DB="${RELAY2_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY2_PASSPHRASE}"

cmd ${FREETON_RELAY} --test-swap test-swap-10${TON_SWAP}.json

export RELAY=3
export TON_MERGE_DB="${RELAY3_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY3_PASSPHRASE}"

cmd ${FREETON_RELAY} --test-swap test-swap-10${TON_SWAP}.json
