open Ezjs_min

let timer = ref None

let set_timeout f time_s =
  Dom_html.window##setTimeout (wrap_callback f) (float_of_int time_s *. 1000.)

let clear_timeout id =
  Dom_html.window##clearTimeout id

let clear_timer () = match !timer with
  | None -> ()
  | Some t -> Dom_html.window##clearInterval t

let create_timer time_s f =
  let t = Dom_html.window##setInterval
      (wrap_callback f)
      (float_of_int time_s *. 1000.) in
  timer := Some t
