open Js_of_ocaml

let dun_units = 1_000_000L

let sep =
  try
    match Js.to_string (Js.number_of_float 0.1)##toLocaleString with
    | "0.1" -> '.'
    | "0,1" -> ','
    | _ -> '.'
  with _ -> '.'

let non_zeroes s =
  let len = String.length s in
  let rec iter s n len =
    match s.[n-1] with
    | '0' -> iter s (n-1) len
    | '.' | ',' -> n-1
    | _ -> n
  in
  iter s len len

let pp_amount0 ?(precision=6) ?(width=15) ?order volumeL =
  if volumeL = 0L then "0", 2, None
  else
    let sign = if volumeL < 0L then "-" else "" in
    let volumeL = Int64.abs volumeL in
    let ndecimal = String.length (Int64.to_string volumeL) in
    let diff_length = ndecimal - width in
    let order = match order with
      | None -> if ndecimal > 6 then
          max 2 ((ndecimal - width + 2) / 3)
        else if ndecimal > 3 then
          max 1 ((ndecimal - width + 2) / 3)
        else
          max 0 ((ndecimal - width + 2) / 3)
      | Some order -> order in
    let unit_float = if diff_length < 0 then 1L else
        Int64.(of_float (10. ** (float diff_length))) in
    let unit_int = if order < 0 then 1L else
        Int64.(of_float (10. ** (float (order * 3 )))) in
    let volume_float = Int64.div volumeL unit_float in
    let volume_int = Int64.div volumeL unit_int in
    let decimal = Int64.sub (Int64.mul volume_float unit_float)
        (Int64.mul volume_int unit_int) in
    let volume_int, decimal, precision =
      if order * 3 < precision then volume_int, decimal, order * 3 else
        let unit_precision = Int64.of_float (10. ** float (order * 3 - precision)) in
        let tmp = Int64.div decimal unit_precision in
        let unit_precision2 = Int64.div unit_precision 10L in
        if unit_precision2 = 0L || tmp = 0L ||
           Int64.div (Int64.sub decimal (Int64.mul tmp unit_precision))
             (Int64.div unit_precision 10L) < 5L then
          volume_int, tmp, precision
        else if Int64.succ tmp = Int64.of_float (10. ** float precision) then
          Int64.succ volume_int, Int64.zero, precision
        else
          volume_int, Int64.succ tmp, precision
    in
    let num = Js.to_string
        ((Js.number_of_float
            (Int64.to_float volume_int))##toLocaleString ) in
    let s = Printf.sprintf "%s" num  in
    let decimal_str = Printf.sprintf "%c%0*Ld" sep precision decimal in
    let n = non_zeroes decimal_str in
    let decimal_str = String.sub decimal_str 0 n in
    sign ^ s, max order 0,
    if decimal <> 0L then Some decimal_str else None

let dun_str = "\196\145"
let dollar_str = "\036"
let mu_str = "\194\181"

let symbol_str ?(order=2) ?(symbol=dun_str) () =
  let units = [|"\194\181"; "m"; ""; "K"; "M"; "B"; "T"|] in
  if order < 0 || order > 6 then ""
  else units.(order) ^ symbol

let ton_symbol_str ?(order=2) () =
  if order < 0 || order > 6 then ""
  else fst @@ List.nth Extraton.units order
