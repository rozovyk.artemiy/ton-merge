#!/bin/sh

BINDIR=$(dirname $0)/../bin

echo "Command: $BINDIR/$*"

while true; do
    DATE=$(date)
    echo "$DATE: Calling $BINDIR/$*"
    $BINDIR/$*
    DATE=$(date)
    echo "$DATE: Process exited with code $?"
    echo "$DATE: Waiting for 60 seconds"
    sleep 3
done
